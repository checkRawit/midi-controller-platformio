#include <Arduino.h>

#include "myDisplay.h"

/* ---------------------------------- DEBUG --------------------------------- */

void loopReport(char *message) {
    while (true) {
        Serial.println(message);
        delay(1000);
    }
}

/* ----------------------------- MIDI Controller ---------------------------- */

#include <MIDIController.h>

MidiController::MidiController midi_controller;
int bank_current;

inline MidiType getMidiType(int val) {
    switch (val) {
        case 0xC0:
            return ProgramChange;
        case 0xB0:
            return ControlChange;
        default:
            return InvalidType;
    }
}

int getIntType(MidiType val) {
    switch (val) {
        case ProgramChange:
            return 0xC0;
        case ControlChange:
            return 0xB0;
        default:
            return 0;
    }
}

/* --------------------------------- Button --------------------------------- */

#include <Bounce2.h>
#include "ButtonController.h"

#define BUTTON_ITEM 4
const int BUTTON_PIN[BUTTON_ITEM] = {D3, D5, D6, D7};

Bounce button[BUTTON_ITEM];
ButtonController::ButtonController btn_controller(4);

void button_updater() {
    for (int index = 0; index < BUTTON_ITEM; index++) {
        button[index].update();
    }
}

inline void button_setup() {
    for (int index = 0; index < BUTTON_ITEM; index++) {
        pinMode(BUTTON_PIN[index], INPUT_PULLUP);
        button[index] = Bounce(BUTTON_PIN[index], 50);
        btn_controller.registerButton(
            index, [index]() { return !(button[index].read()); });
    }

    /* register sending midipreset action */
    for (int index = 0; index < BUTTON_ITEM; index++) {
        int button_index_arr[] = {index};
        btn_controller.registerAction(button_index_arr, 1, [index]() {
            midi_controller.sendMessage(index);
        });
    }

    /* register Bank up action */
    int button_index_arr[] = {0, 3};
    btn_controller.registerAction(button_index_arr, 2, []() {
        midi_controller.bankUp();
        bank_current = midi_controller.getBankCurrent();
        Serial.print("Current Bank: ");
        Serial.println(midi_controller.midibank[bank_current].getName());

        /* Display Bank index */
        myDisplay.clear();
        myDisplay.drawString(0, 0, String(midi_controller.getBankCurrent()));
        myDisplay.display();
    });

    /* Optional: register button updater action */
    btn_controller.registerUpdater(button_updater);
}

/* ---------------------------- SPIF File System ---------------------------- */

#include "FS.h"

/* ---------------------------------- JSON ---------------------------------- */

#include <ArduinoJson.h>

void saveBankJson(int bank_index) {
    // Open file for writing
    char file_name[30];
    sprintf(file_name, "/midibank/%02d.json\0", bank_index);
    File file = SPIFFS.open(file_name, "w");
    if (!file) {
        loopReport("Can't open file for writing.");
    }
    //*/

    DynamicJsonBuffer json_buffer(2000);
    JsonObject &bank_json = json_buffer.createObject();
    bank_json["name"] = midi_controller.midibank[bank_index].getName();
    JsonArray &midipreset_arr_json = bank_json.createNestedArray("midipreset");
    for (int preset_index = 0;
         preset_index <
         midi_controller.midibank[bank_index].getMidiPresetAmount();
         preset_index++) {
        JsonObject &midipreset_json = midipreset_arr_json.createNestedObject();
        JsonArray &midimessage_arr_json =
            midipreset_json.createNestedArray("midimessage");
        for (int message_index = 0;
             message_index < midi_controller.midibank[bank_index]
                                 .midipreset[preset_index]
                                 .getMidiMessageAmount();
             message_index++) {
            const MidiController::MidiMessage *midimessage =
                midi_controller.midibank[bank_index]
                    .midipreset[preset_index]
                    .getMidiMessage(message_index);
            JsonObject &midimessage_json =
                midimessage_arr_json.createNestedObject();
            midimessage_json["type"] = getIntType(midimessage->inType);
            midimessage_json["data1"] = midimessage->inData1;
            midimessage_json["data2"] = midimessage->inData2;
            midimessage_json["channel"] = midimessage->inChannel;
        }
    }
    bank_json.prettyPrintTo(file);
    file.close();
}

void getBankJsonString(char *bankJson_string, int char_size, int bank_index) {
    DynamicJsonBuffer json_buffer(2000);
    JsonObject &bank_json = json_buffer.createObject();
    bank_json["name"] = midi_controller.midibank[bank_index].getName();
    JsonArray &midipreset_arr_json = bank_json.createNestedArray("midipreset");
    for (int preset_index = 0;
         preset_index <
         midi_controller.midibank[bank_index].getMidiPresetAmount();
         preset_index++) {
        JsonObject &midipreset_json = midipreset_arr_json.createNestedObject();
        JsonArray &midimessage_arr_json =
            midipreset_json.createNestedArray("midimessage");
        for (int message_index = 0;
             message_index < midi_controller.midibank[bank_index]
                                 .midipreset[preset_index]
                                 .getMidiMessageAmount();
             message_index++) {
            const MidiController::MidiMessage *midimessage =
                midi_controller.midibank[bank_index]
                    .midipreset[preset_index]
                    .getMidiMessage(message_index);
            JsonObject &midimessage_json =
                midimessage_arr_json.createNestedObject();
            midimessage_json["type"] = getIntType(midimessage->inType);
            midimessage_json["data1"] = midimessage->inData1;
            midimessage_json["data2"] = midimessage->inData2;
            midimessage_json["channel"] = midimessage->inChannel;
        }
    }
    bank_json.printTo(bankJson_string, char_size);
}

/* ---------------------------------- WIFI ---------------------------------- */

#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

const char *server_ssid = "MyDevice";
const char *server_password = "MyPassword";

ESP8266WebServer server(80);

void handleRoot() {
    String response = "<h1>You are connected</h1>";
    if (server.hasArg("plain")) {
        Serial.println(server.arg("plain"));
        response += "<h2>Body Received</h2>";
        response += server.arg("plain");
    }
    server.send(200, "text/html", response);
}

void handleBankJson() {
    char bank_json[1000];
    getBankJsonString(bank_json, 1000, midi_controller.getBankCurrent());
    server.send(200, "application/json", bank_json);
}

/* -------------------------------------------------------------------------- */

void setup() {
    /* Serial for debug Setup */
    Serial.begin(115200);
    Serial.println("Begin setup");

    /* Button Setup */
    button_setup();

    myDisplay_setup();

    /* WiFi */
    WiFi.softAP(server_ssid, server_password);
    server.on("/", handleRoot);
    server.on("/api/bankjson", handleBankJson);
    server.begin();
    Serial.println(WiFi.softAPIP());  // print Server IP Address

    /* File Setup */
    // Loading save.json SPIFFS
    midi_controller.begin();
    SPIFFS.begin();
    for (int bank_index = 0;; bank_index++) {
        // Load Next MidiBank JSON file
        char file_name[30];
        sprintf(file_name, "/midibank/%02d.json\0", bank_index);
        Serial.print(file_name);
        File saveJsonFile = SPIFFS.open(file_name, "r");
        if (!saveJsonFile) {  // break the loop if file not exists
            Serial.println(" not found");
            midi_controller.bankUp();  // Go to first MidiBank
            bank_current = midi_controller.getBankCurrent();
            break;
        }

        // Parse to JSON Object
        DynamicJsonBuffer json_buffer(1500);
        JsonObject &bank_json = json_buffer.parseObject(saveJsonFile);
        saveJsonFile.close();

        // Create a new MidiBank
        if (!midi_controller.insertBank()) {
            loopReport("MidiController_bank is full.");
        }
        midi_controller.bankUp();
        bank_current = midi_controller.getBankCurrent();

        JsonArray &midipreset_arr_json = bank_json["midipreset"];
        // TODO: Checking midipreset_amount
        midi_controller.midibank[bank_current].init(midipreset_arr_json.size());
        midi_controller.midibank[bank_current].setName(bank_json["name"]);
        for (int preset_index = 0; preset_index < midipreset_arr_json.size();
             preset_index++) {
            JsonArray &midimessage_arr_json =
                midipreset_arr_json[preset_index]["midimessage"];
            midi_controller.midibank[bank_current]
                .midipreset[preset_index]
                .init(midimessage_arr_json
                          .size());  // TODO: Checking midimessage_amount
            for (int message_index = 0;
                 message_index < midimessage_arr_json.size(); message_index++) {
                JsonObject &midimessage_json =
                    midimessage_arr_json[message_index];
                MidiType type = getMidiType(midimessage_json["type"].as<int>());
                DataByte data1 = midimessage_json["data1"].as<byte>();
                DataByte data2 = midimessage_json["data2"].as<byte>();
                Channel channel = midimessage_json["channel"].as<byte>();
                midi_controller.midibank[bank_current]
                    .midipreset[preset_index]
                    .load(message_index, type, data1, data2, channel);
            }
        }
        Serial.println(" Loaded");
    }

    Serial.println("End of Setup Function");
    Serial.print("Current Bank: ");
    Serial.println(midi_controller.midibank[bank_current].getName());

    // Display
    myDisplay.clear();
    myDisplay.drawString(0, 0, String(midi_controller.getBankCurrent()));
    myDisplay.display();
}

void loop() {
    // TODO: test saveBankJson()
    btn_controller.handle();
    if (Serial.available()) {
        Serial.println("# Process...");
        DynamicJsonBuffer json_input_buffer(200);
        JsonObject &json_input = json_input_buffer.parseObject(Serial);
        if (json_input["cmd"] == "set_name") {
            midi_controller.midibank[midi_controller.getBankCurrent()].setName(
                json_input["arg1"]);
        }
        Serial.println("# Done");
    }

    /* Wifi */
    server.handleClient();
}

/* -------------------------------------------------------------------------- */