#include <Arduino.h>
#include <SSD1306.h>
#include "myDisplay.h"
#include "font.h"

SSD1306 myDisplay(0x3c, OLED_SDA_PIN, OLED_SCL_PIN);

void myDisplay_setup() {
    myDisplay.init();
    myDisplay.setFont(Nimbus_Sans_L_Bold_Condensed_48);
}