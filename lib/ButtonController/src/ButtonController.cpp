#include "Arduino.h"
#include "ButtonController.h"

namespace ButtonController {
    ButtonController::ButtonController(int button_amount) {
        _button_amount = button_amount;
    }

    void ButtonController::registerAction(int action_index, std::function<void()> action) {
        _button_action[action_index] = action;
    }

    void ButtonController::registerAction(int button_index_arr[], int arr_size, std::function<void()> action) {
        if (arr_size == 0) return;
        
        int action_index = 0;
        for (int i = 0; i < arr_size; i++) {
            action_index |= 1 << button_index_arr[i];
        }
        registerAction(action_index, action);
    }

    void ButtonController::registerButton(int button_index, std::function<int()> button_reader) {
        _button_reader[button_index] = button_reader;
    }

    void ButtonController::registerUpdater(std::function<void()> updater) {
        _button_updater = updater;
    }

    void ButtonController::handle() {
        // for checking button rising
        _button_status_prev = _button_status;

        // Optional: function for updating button status
        _update();

        /* Managing button status */
        for (int button_index = 0; button_index < _button_amount; button_index++) {
            if (_button_reader[button_index]() == 1) {
                _button_status |= 1 << button_index;
                // If button status is changed from 0 to 1
                if (!((_button_status_prev >> button_index) & 1)) {
                    _button_pressed |= 1 << button_index;
                }
            } else {
                _button_status &= ~(1 << button_index);
            } 
        }

        if (_button_status == 0) {
            if (_button_pressed != 0) {

                // Remove "#include "Arduino.h" if it is not used"
                Serial.print("# Button_pressed: ");
                Serial.println(_button_pressed);

                // Call action by if action is available
                if (_button_action[_button_pressed] != NULL) {
                    _button_action[_button_pressed](); 
                }
                // Reset value
                _button_pressed = 0;
            }
        }
    }

    // Optional: function for updating button status
    void ButtonController::_update() {
        if (_button_updater != NULL) {
            _button_updater();
        }
    }
}