#ifndef ButtonController_h
#define ButtonController_h

#include <functional>

namespace ButtonController {

    class ButtonController {
        public:
            ButtonController(int button_amount);
            void handle();
            void registerButton(int button_index, std::function<int()> button_reader);
            void registerAction(int action_index, std::function<void()> action);
            void registerAction(int button_index_arr[], int arr_size, std::function<void()> action);
            void registerUpdater(std::function<void()> updater);

        private:
            int _button_amount;
            int _button_status = 0;
            int _button_status_prev = 0;
            int _button_pressed = 0;
            std::function<int()> _button_reader[32] = {};
            std::function<void()> _button_action[32] = {};
            std::function<void()> _button_updater = NULL;

            void _update();
    };
    
}

#endif