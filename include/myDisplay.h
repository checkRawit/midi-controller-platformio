#ifndef myDisplay_h
#define myDisplay_h

#include <SSD1306.h>

#define OLED_SDA_PIN D2
#define OLED_SCL_PIN D1
#define OLED_TEXT_LENGTH 7

extern SSD1306 myDisplay;

void myDisplay_setup();

#endif